use rslnp::{ErrorKind, Parser};

fn test_error(args: &str, expected: ErrorKind) {
    assert_eq!(
        Parser::scopes().parse(args.chars()).err().unwrap().kind,
        expected
    )
}

fn test_error_indentation(args: &str, expected: usize) {
    let error = Parser::scopes().parse(args.chars()).err().unwrap();
    assert_eq!(error.column, expected);
    assert_eq!(error.kind, ErrorKind::Indentation);
}

#[test]
fn unmatched_brackets() {
    test_error("[}", ErrorKind::UnmatchedBrackets('[', '}'));
}

#[test]
fn unclosed_brackets() {
    test_error("(", ErrorKind::UnclosedBracket);
}

#[test]
fn too_many_brackets() {
    test_error(")", ErrorKind::TooManyBrackets);
}

#[test]
fn unclosed_string() {
    test_error("\"", ErrorKind::UnclosedString);
}

#[test]
fn unclosed_string_newline() {
    test_error("\"\n", ErrorKind::UnclosedString);
}

#[test]
fn multiline_prefix_length() {
    test_error("\"\"\"", ErrorKind::MultilinePrefixLength);
}

#[test]
fn multiline_prefix_length_newline() {
    test_error("\"\"\"\n", ErrorKind::MultilinePrefixLength);
}

#[test]
fn low_indentation() {
    test_error_indentation("test\n   test", 3);
}

#[test]
fn high_indentation() {
    test_error_indentation("test\n     test", 5);
}

#[test]
fn multi_indent() {
    test_error_indentation("test\n            test", 12);
}

#[test]
fn unescapeable_character() {
    test_error("\"\\a\"", ErrorKind::UnescapeableCharacter('a'));
}
